import { Fragment, useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './components/CourseView';
import Register from './pages/Register';
import Login from './pages/Login';
import PageNotFound from './pages/PageNotFound';
import Logout from './pages/Logout';
import './App.css';
import { UserProvider } from './UserContext';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';

function App() {
  // State hook for the user state
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })


  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user info is properly stored during login and local storage info is cleared upon logout.

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
  <UserProvider value={{user, setUser, unsetUser}}>
   <Router>
     <AppNavbar />
     <Container>
       <Routes>
       <Route path="/" element={<Home />} />
       <Route path="/courses" element={<Courses />} />
       <Route path="/courses/:courseId" element={<CourseView />} />
       <Route path="/login" element={<Login />} />
       <Route path="/register" element={<Register />} />
       <Route path="/logout" element={<Logout />} />
       <Route path="*" element={<PageNotFound />} />
       </Routes>
     </Container>
   </Router>
   </UserProvider>
  );
}

export default App;
