import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Check if values are successfully binded
    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNo);
    console.log(password1);
    console.log(password2);

    const checkUserEmail = (email) => {
        fetch('http://localhost:4000/users/checkEmail/', {
            method: 'POST',
            headers: {
                "Content-Type" : "application/json",
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === false){
                fetch('http://localhost:4000/users/register', {
                    method: 'POST',
                    headers: {
                        'Content-Type' : 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                })

                Swal.fire({
                    title : "Registration successful",
                    icon : "success",
                    text : "Welcome to Zuitt!"
                })
                navigate("/login");
            }
            else{
                Swal.fire({
                    title : "Duplicate email found",
                    icon : "error",
                    text : "Please provide a different email."
                })
            }
        })
    }

    function registerUser(e){
        e.preventDefault();

        // clear input fields
        setFirstName('');
        setLastName('');
        setEmail('');
        setMobileNo('');
        setPassword1('');
        setPassword2('');
    }

    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' &&  password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [firstName, lastName, email, mobileNo, password1, password2]);

    return(
        <Form onSubmit={(e) => registerUser(e)}>
            <h1>Register</h1>
            <Form.Group className="mb-3" controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="string"
                    placeholder="Enter first name" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="string"
                    placeholder="Enter last name" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email"
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="tel"
                    placeholder="Enter Mobile Number" 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                type="password" 
                placeholder="Password" 
                value={password1}
                onChange={e => setPassword1(e.target.value)}
                required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>
            {/* Conditionally render submit button based on isActive state */}
            { isActive ?
                <Button variant="primary" type="submit" id="submitBtn" onClick={() => checkUserEmail(email)}>
                Submit
                </Button>
            :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
            }

        </Form>
    )
}

