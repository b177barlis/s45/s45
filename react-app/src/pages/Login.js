import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Login() {

	// Allows us to consume the User context object and its properties to use for user validation
	const { user, setUser } = useContext(UserContext);
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(true);


	function LoginUser(e){

		// Prevents page redirection via form submission
		e.preventDefault();

		// Set the email of the user in the local storage
		
		fetch('http://localhost:4000/users/login', {
						method: 'POST',
		                headers: {
		                    'Content-Type': 'application/json'
		                },
		                body: JSON.stringify({
		                    email: email,
		                    password: password
		                })

		})
		.then(res => res.json())
		.then(data => {
							console.log(data)

			                if(typeof data.access !== "undefined"){
			                    // The JWT will be used to retrieve user information
			                    localStorage.setItem('token', data.access);
			                    retrieveUserDetails(data.access);
			                    // Sweet alert message
			                    Swal.fire({
			                        title: "Login Successful",
			                        icon: "success",
			                        text: "Welcome to Zuitt!"
			                    })
			                }
			                else{
			                    Swal.fire({
			                        title: "Authentication failed",
			                        icon: "error",
			                        text: "Check your login details and try again."
			                    })
			                }

		})


		// Clear input fields after submission
		setEmail('');
		setPassword('');

	}


	  // "retrieveUserDetails" function to convert JWT from the fetch request
        const retrieveUserDetails = (token) => {
            fetch('http://localhost:4000/users/details',{
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        }


	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match
		if(email !== '' && password !== '') {
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password]);


	return (
			(user.id !== null) ?
			<Navigate to="/courses" />
			:
			<Form onSubmit={(e) => LoginUser(e)}>
			<h1>Login</h1>
			  <Form.Group className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value = {email}
			    	onChange = {e => setEmail(e.target.value)}
			    	required
			    />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password}
			    	onChange = {e => setPassword(e.target.value)}
			    	required
			    	/>
			  </Form.Group>

			{/*Conditionally render submit button based on isActive state*/}
			{ isActive ? 
			  <Button variant="primary" type="submit" id="submitBtn">
			    Submit
			  </Button>

			  :

			  <Button variant="danger" type="submit" id="submitBtn" disabled>
			    Submit
			  </Button>
			}
			</Form>
		)
			
}