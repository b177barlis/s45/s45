import { Button, Row, Col } from 'react-bootstrap';

export default function PageNotFound(){
    return (
        <Row>
            <Col className="p-5">
                <h1>Page Not Found</h1>
                <p>Go back to <a href="/">homepage.</a></p>
            </Col>
        </Row>
    )
}
